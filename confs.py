def cu_param(cu_phy):
    name_param = {
        1: 'KAIN-FRITSCH',
        2: 'BETTS-MILLER-JANJIC',
        3: 'GRELL-FREITAS',
        5: 'GRELL-3D',
        11: 'MULTI-SCALE-KAIN-FRITSCH'
    }
    return name_param[cu_phy]

def model_version(model):
    model = model.replace(" ", "")  # remove espaços da string
    modelv = {
        'OUTPUTFROMWRFV3.9.1.1MODEL': 'V3',
        'OUTPUTFROMWRFV4.0MODEL': 'V4'
    }
    return modelv[model]

def weekday(num):
    day_name = {0:'SEG', 1:'TER', 2:'QUA', 3:'QUI',
                4:'SEX', 5:'SAB', 6:'DOM'}
    return day_name[num]

def grid_limits(opt=1):
    limits = {1: [-51., -27., -22., 5.]}
    return limits[opt]
