# para rodar: streamlit run app.py

import streamlit as st

st.sidebar.title("PREVISÃO DE TEMPO")

model_choice = st.sidebar.selectbox(
    "MODELO DE TEMPO:",
    ["WRF3.0", "WRF4.0"])  # "RAMS6.0",

date_choice = st.sidebar.date_input(
    "DATA DA INICIALICAÇÃO:",
    date(2019, 10, 11))

ini_data_choice = st.sidebar.radio(
    'HORÁRIO DA INICIALIZAÇÃO', ('00', '12'), 0)

var_choice = st.sidebar.selectbox(
    "VARIÁVEIS DO MODELO:",
    ["PRECIPITAÇÃO ACUMULADA 24H",
     "TEMPERATURA MÍNIMA DIÁRIA",
     "TEMPERATURA MÉDIA  DIÁRIA",
     "TEMPERATURA MÁXIMA DIÁRIA",
     "UMIDADE RELATIVA A 2M",
     "VORTICIDADE"])

model_name = {
    'WRF3.0': 'wrf',
    'WRF4.0': 'wrf'
    }

model_version = {
    'WRF3.0': 'v3',
    'WRF4.0': 'v4'
    }

x = st.info("Loading...")

st.image(open("img.png", "rb").read(),
         caption="from read",
         format="png",
         use_column_width=False)
