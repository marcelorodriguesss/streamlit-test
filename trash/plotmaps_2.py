import numpy as np
import cartopy as cart
# import matplotlib as mpl
# mpl.use('Agg')
from matplotlib import pyplot as plt
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
from matplotlib import colors as c

class PlotMaps:

    def plotmaps(self, data, lat, lon, **kwargs):

        self.plot_type   = kwargs.get('plot_type', 'pcolormesh')
        self.clevs       = kwargs.get('clevs', None)
        self.cmap_over   = kwargs.get('cmap_over', '#ffffff')
        self.cmap        = kwargs.get('cmap', '#ffffff')
        self.cmap_under  = kwargs.get('cmap_under', '#ffffff')
        self.barinf      = kwargs.get('barinf', 'neither')  # 'neither', 'both', 'max', 'min'
        self.fig_title   = kwargs.get('fig_title', '')
        self.fig_botton  = kwargs.get('fig_botton', '')
        self.fig_name    = kwargs.get('fig_name', 'output.png')
        self.cu_param    = kwargs.get('cu_param', 1)
        self.dpi         = kwargs.get('dpi', 100)
        self.units       = kwargs.get('units', 'no_units')
        self.grid_limits = kwargs.get('grid_limits', None)

        # ajuste na grade para plot pcolormesh
        if self.plot_type == 'pcolormesh':
            self.deltalat = np.mean(np.diff(lat)) / 2.
            self.deltalon = np.mean(np.diff(lon)) / 2.
            self.lat = lat - self.deltalat
            self.lon = lon - self.deltalon

        # definição de paleta de cores
        self.cmap = c.ListedColormap(self.cmap)
        self.cmap.set_over(self.cmap_over)
        self.cmap.set_under(self.cmap_under)
        self.norm = BoundaryNorm(self.clevs, ncolors=self.cmap.N, clip=False)

        # outros ajustes no plot
        fig = plt.figure(figsize=(7, 7), dpi=self.dpi, )

        # f, axs = plt.subplots(2,2,figsize=(15,15))
        # f = plt.figure(figsize=(10,3))
        # ax = f.add_subplot(121)
        # ax2 = f.add_subplot(122)

        ax = plt.axes(projection=cart.crs.PlateCarree(central_longitude=300))

        img = ax.pcolormesh(self.lon, self.lat, data, cmap=self.cmap, norm=self.norm,
                            transform=cart.crs.PlateCarree())

        # formatando os ticks de latitudes e longitudes
        # https://climate-cms.org/2018/05/01/latlon-reference.html
        # https://stackoverflow.com/questions/49956355/adding-gridlines-using-cartopy
        g1 = ax.gridlines(xlocs=range(-180, 181, 4), ylocs=range(-90, 91, 2),
                          draw_labels=True, linewidth=0.01)
        g1.xlabels_top = False
        g1.ylabels_right = False
        g1.xformatter = cart.mpl.gridliner.LONGITUDE_FORMATTER
        g1.yformatter = cart.mpl.gridliner.LATITUDE_FORMATTER
        g1.xlabel_style = {'color': 'k', 'weight': 'bold', 'size': 9}
        g1.ylabel_style = {'color': 'k', 'weight': 'bold', 'size': 9}

        # contorno dos estados
        states = cart.feature.NaturalEarthFeature(
            category='cultural',
            scale='50m', facecolor='none',
            name='admin_1_states_provinces_shp')
        ax.add_feature(states, edgecolor='k')

        # contorno dos países
        countries = cart.feature.NaturalEarthFeature(
            category='cultural',
            scale='50m', facecolor='none',
            name='admin_0_countries')
        ax.add_feature(countries, edgecolor='k')

        # limites da grade
        # Oeste/Leste/Sul/Norte
        ax.set_extent(self.grid_limits, cart.crs.PlateCarree())

        # definindo paleta de cores
        bar = fig.colorbar(img, extend=self.barinf, shrink=0.94, pad=0.02,
                           spacing='uniform', orientation='vertical',
                           ax=ax, ticks=self.clevs, extendfrac='auto')
        bar.set_label(label=f'({self.units})', size=10, weight='bold')
        bar.ax.tick_params(labelsize=10)
        bar.ax.set_yticklabels(self.clevs, fontsize=9, weight='bold')

        # definindo titulo da figura
        ax.set_title(self.fig_title, fontsize=9, weight='bold', loc='left')

        # definindo título rodapé da figura
        ax.text(0.5, -0.13, self.fig_botton,
                va='bottom', ha='center', rotation='horizontal',
                rotation_mode='anchor', transform=ax.transAxes,
                fontsize=12, weight='bold')

        # definindo nomes de latitude e longitude nos eixos da figura
        ax.text(-0.10, 0.5, 'LATITUDE', va='bottom', ha='center',
                rotation='vertical', rotation_mode='anchor',
                transform=ax.transAxes, fontsize=10, weight='bold')
        ax.text(0.5, -0.08, 'LONGITUDE', va='bottom', ha='center',
                rotation='horizontal', rotation_mode='anchor',
                transform=ax.transAxes, fontsize=10, weight='bold')

        plt.tight_layout()

        # plt.show()
        # plt.savefig(self.fig_name, dpi=self.dpi, bbox_inches='tight')
        # plt.close()

        return fig
