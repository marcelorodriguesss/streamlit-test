import numpy as np
import cartopy as cart
# import matplotlib as mpl
# mpl.use('Agg')
from matplotlib import pyplot as plt
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
from matplotlib import colors as c

class PlotMaps:

    def precip(self, data_models, **kwargs):

        self.grid_limits = kwargs.get('grid_limits', None)

        data =  data_models['model_0'][0]  # data
        data1 =  data_models['model_0'][1]  # title
        data2 =  data_models['model_0'][2]  # phy

        self.clevs = (0., 1., 2., 3., 4., 5., 10., 15., 20.,
                      25., 30., 40., 50., 60., 70., 80.)

        self.cmap = ('#FFFFFF', '#EDEDED', '#E1E1E1', '#4681C9', '#6A55FF',
                     '#3650CB', '#1A758A', '#029A4B', '#42B338', '#83CD25',
                     '#C3E711', '#FFF700', '#FF9F00', '#FF4600', '#ED0000')

        self.cmap_over = '#990000'

        lat = data.coords['lat']
        lon = data.coords['lon']

        # TODO: Verificar se Cartopy já faz o ajuste
        self.deltalat = np.mean(np.diff(lat)) / 2.
        self.deltalon = np.mean(np.diff(lon)) / 2.
        self.lat = lat - self.deltalat
        self.lon = lon - self.deltalon

        # definição de paleta de cores
        self.cmap = c.ListedColormap(self.cmap)
        self.cmap.set_over(self.cmap_over)
        self.norm = BoundaryNorm(self.clevs, ncolors=self.cmap.N, clip=False)

        # outros ajustes no plot
        # fig = plt.figure(figsize=(7, 7), dpi=self.dpi, )
        # f, axs = plt.subplots(2, 2, figsize=(7, 7), dpi=90)

        fig = plt.figure(figsize=(7, 7), dpi=90)

        ax1 = fig.add_subplot(121)
        # ax2 = f.add_subplot(122)

        ax1 = plt.axes(projection=cart.crs.PlateCarree(central_longitude=300))

        img1 = ax1.pcolormesh(self.lon, self.lat, data[0, ...], cmap=self.cmap,
                              norm=self.norm, transform=cart.crs.PlateCarree())

        # formatando os ticks de latitudes e longitudes
        # https://climate-cms.org/2018/05/01/latlon-reference.html
        # https://stackoverflow.com/questions/49956355/adding-gridlines-using-cartopy

        g1 = ax1.gridlines(xlocs=range(-180, 181, 4), ylocs=range(-90, 91, 2),
                           draw_labels=True, linewidth=0.01)
        g1.xlabels_top = False
        g1.ylabels_right = False
        g1.xformatter = cart.mpl.gridliner.LONGITUDE_FORMATTER
        g1.yformatter = cart.mpl.gridliner.LATITUDE_FORMATTER
        g1.xlabel_style = {'color': 'k', 'weight': 'bold', 'size': 9}
        g1.ylabel_style = {'color': 'k', 'weight': 'bold', 'size': 9}

        # contorno dos estados
        states = cart.feature.NaturalEarthFeature(
            category='cultural',
            scale='50m', facecolor='none',
            name='admin_1_states_provinces_shp')
        ax1.add_feature(states, edgecolor='k')

        # contorno dos países
        countries = cart.feature.NaturalEarthFeature(
            category='cultural',
            scale='50m', facecolor='none',
            name='admin_0_countries')
        ax1.add_feature(countries, edgecolor='k')

        # limites da grade
        # Oeste/Leste/Sul/Norte
        # ax1.set_extent(self.grid_limits, cart.crs.PlateCarree())

        # definindo paleta de cores
        bar1 = fig.colorbar(img1, extend='max', shrink=0.94, pad=0.02,
                            spacing='uniform', orientation='vertical',
                            ax=ax1, ticks=self.clevs, extendfrac='auto')
        bar1.set_label(label=f'(mm)', size=10, weight='bold')
        # bar1.tick_params(labelsize=10)
        # bar1.set_yticklabels(self.clevs, fontsize=9, weight='bold')

        # definindo titulo da figura
        self.fig_title = f"FUNCEME/WRF_model_version/GFS/KF\n" \
                         f"INICIALIZADA EM: ini_date\n" \
                         f"VÁLIDA PARA: DD/MM/YYYY HH:MM (weekday) (GMT-3)"
        ax1.set_title(self.fig_title, fontsize=9, weight='bold', loc='left')

        # definindo título rodapé da figura
        self.fig_botton_title = 'PRECIPITAÇÃO ACUMULADA NAS ÚLTIMAS 24H'
        ax1.text(0.5, -0.13, self.fig_botton_title,
                va='bottom', ha='center', rotation='horizontal',
                rotation_mode='anchor', transform=ax1.transAxes,
                fontsize=12, weight='bold')

        # definindo nomes de latitude e longitude nos eixos da figura
        ax1.text(-0.10, 0.5, 'LATITUDE', va='bottom', ha='center',
                rotation='vertical', rotation_mode='anchor',
                transform=ax1.transAxes, fontsize=10, weight='bold')
        ax1.text(0.5, -0.08, 'LONGITUDE', va='bottom', ha='center',
                rotation='horizontal', rotation_mode='anchor',
                transform=ax1.transAxes, fontsize=10, weight='bold')

        plt.tight_layout()

        # plt.show()
        # plt.savefig(self.fig_name, dpi=self.dpi, bbox_inches='tight')
        # plt.close()

        return fig
