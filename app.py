#
# para rodar: streamlit run app.py
#

import streamlit as st
from datetime import date, datetime, timedelta
import os

# FUNCTION TO ALWAYS START IN WIDE MODE (2000PX WIDTH)
def _max_width_():
    max_width_str = f"max-width: 2000px;"
    st.markdown(
        f'''
        <style>
        .reportview-container .main .block-container{{
            {max_width_str}
        }}
        </style>
        ''',
        unsafe_allow_html=True,
        )

# FUNCTION TO GENERATE FIGURE NAME
def fig_name(figs_dir, date_choice, region_choice, ini_date_choice,
             data, num1, num2):
    d1 = date_choice + timedelta(days = num1)
    d2 = date_choice + timedelta(days = num2)
    date_choice_actual = date_choice.strftime('%Y%m%d')
    date_choice_1 = d1.strftime('%Y%m%d')
    date_choice_2 = d2.strftime('%Y%m%d')

    return f"{figs_dir}/gfs-week-{date_choice_actual}{ini_date_choice}/" \
           f"{region_choice}_pr_{data}_" \
           f"{date_choice_actual}_{date_choice_1}_" \
           f"{date_choice_2}_{ini_date_choice}_gfs025.png"

_max_width_()

home = os.path.expanduser("~")

st.sidebar.title("PREVISÃO DE TEMPO")

model_choice = st.sidebar.selectbox(
    "MODELO DE TEMPO:",
    ["WRF4.0", "ENSEMBLE", "GFS 25KM",
    "PROBABILIDADE DE PRECIPITAÇÃO", "PREVISÃO DE DOL"]  # "RAMS6.0"
    )

date_choice = st.sidebar.date_input(
    "DATA DA INICIALICAÇÃO:",
    date.today()  # date(2019, 10, 11)
    )

if date_choice > date.today():
    st.sidebar.error('VERIFIQUE A DATA DE INICIALIZAÇÃO!')

# INITIAL HOUR
if model_choice == "GFS 25KM":
    ini_date_choice = st.sidebar.radio(
        'HORÁRIO DA INICIALIZAÇÃO:', ('00', '12'), 0
    )
elif model_choice == "PREVISÃO DE DOL":
    pass
else:
    ini_date_choice = st.sidebar.radio(
        'HORÁRIO DA INICIALIZAÇÃO:', ('00', '00'), 0
    )

# FREQUENCY
models_with_sinop_vars = ["WRF4.0", "ENSEMBLE"]

models_with_daily_vars = ["WRF4.0", "ENSEMBLE", "GFS 25KM",
                          "PROBABILIDADE DE PRECIPITAÇÃO"]

models_with_no_vars = ["PREVISÃO DE DOL"]

if model_choice in models_with_sinop_vars:
    daily_or_sinop = st.sidebar.radio(
        'FREQUÊNCIA:', ('DIÁRIA', 'SINÓTICA'), 0
        )

    if daily_or_sinop == 'SINÓTICA':
        sinop_days = st.sidebar.radio(
            'DIA:', ('1', '2', '3'), 0
        )
elif model_choice  in models_with_no_vars:
    pass
else:
    daily_or_sinop = st.sidebar.radio(
        'FREQUÊNCIA:', (['DIÁRIA']), 0
        )

# OTHER WIDGETS
if model_choice == 'PROBABILIDADE DE PRECIPITAÇÃO':

    var_choice = "PRECIP. ACUMULADA DIÁRIA (0H)"

elif model_choice == 'GFS 25KM':

    var_choice = st.sidebar.selectbox(
        "VARIÁVEIS DO MODELO:",
        ["PRECIPITAÇÃO"]
        )

    region_choice = st.sidebar.selectbox(
        "REGIÃO:",
        ["CEARÁ", "NORDESTE DO BRASIL", "BRASIL"]
        )

elif model_choice == "PREVISÃO DE DOL":

    # widget caixa de texto explicando a previsão de dol
    st.sidebar.markdown('''
    > _**Descrição:** Diagramas Hovmöller (longitude x tempo; média entre
    linha do Equador e 5°S) do desvio da componente meridional do vento em 850
    hPa (cores), água precipitável, precipitação e omega 500 hPa (contornos).
    O desvio é calculado subtraindo o campo observado (análise) ou previsto
    (previsão) da média sazonal (DJF, MAM, JJA, SON)._
    ''')
    pass

else:

    if daily_or_sinop == 'DIÁRIA':
        var_choice = st.sidebar.selectbox(
            "VARIÁVEIS DO   MODELO:",
            ["PRECIP. ACUMULADA DIÁRIA (0H)",
            "PRECIP. ACUMULADA DIÁRIA (7H)",
            "RADIAÇÃO SOLAR INCIDENTE",
            "TEMP. 2M MÍNIMA DIÁRIA",
            "TEMP. 2M MÉDIA DIÁRIA",
            "TEMP. 2M MÁXIMA DIÁRIA"]
            )
    else:  # SINÓTICA
        var_choice = st.sidebar.selectbox(
            "VARIÁVEIS DO MODELO:",
            ["PRECIP. ACUMULADA A CADA 6H"]
        )

model_name = {
    'WRF4.0': 'wrf',
    'ENSEMBLE': 'ensemble',
    'PROBABILIDADE DE PRECIPITAÇÃO': 'probs'
    }


model_version_name = {
    'WRF4.0': 'v4'
    }

if model_choice == 'PROBABILIDADE DE PRECIPITAÇÃO':

    var_name = {'PRECIP. ACUMULADA DIÁRIA (0H)': 'preciptotal_diaria0'}

    fig_name_aux1 = '3days'

elif model_choice == 'GFS 25KM':

    regions = {
        'CEARÁ': 'ce',
        'NORDESTE DO BRASIL': 'neb',
        'BRASIL': 'sam'
    }

elif model_choice == 'PREVISÃO DE DOL':

    pass

else:

    if daily_or_sinop == 'DIÁRIA':

        var_name = {
            'PRECIP. ACUMULADA DIÁRIA (0H)': 'preciptotal_diaria0',
            'PRECIP. ACUMULADA DIÁRIA (7H)': 'preciptotal_diaria7',
            'RADIAÇÃO SOLAR INCIDENTE': 'rshort_diaria',
            'TEMP. 2M MÍNIMA DIÁRIA': 'temp2mmin_diaria',
            'TEMP. 2M MÉDIA DIÁRIA': 'temp2mmean_diaria',
            'TEMP. 2M MÁXIMA DIÁRIA': 'temp2mmax_diaria'
        }

    else:  # SINÓTICA

        if sinop_days == '1':
            var_name = {
                "PRECIP. ACUMULADA A CADA 6H": "preciptotal_sinotica0"
            }
        if sinop_days == '2':
                var_name = {
                    "PRECIP. ACUMULADA A CADA 6H": "preciptotal_sinotica24"
                }
        if sinop_days == '3':
                var_name = {
                    "PRECIP. ACUMULADA A CADA 6H": "preciptotal_sinotica48"
                }

    if daily_or_sinop == 'DIÁRIA':
        fig_name_aux1 = '3days'
    else:  # SINÓTICA
        fig_name_aux1 = '6h-6h'

var_fold_name = {
    'PRECIP. ACUMULADA DIÁRIA (0H)': 'precip',
    'PRECIP. ACUMULADA DIÁRIA (7H)': 'precip',
    'PRECIP. ACUMULADA A CADA 6H': 'precip',
    'RADIAÇÃO SOLAR INCIDENTE': 'rshort',
    'TEMP. 2M MÍNIMA DIÁRIA': 'temp2m',
    'TEMP. 2M MÉDIA DIÁRIA': 'temp2m',
    'TEMP. 2M MÁXIMA DIÁRIA': 'temp2m'
}

# carrega as figuras

# https://discuss.streamlit.io/t/multiple-images-along-the-same-row/118/3

figs_dir = f'{home}/Build_WRF/funceme/figs'

if model_choice == 'ENSEMBLE':

    fig1 = f"{figs_dir}/{model_name[model_choice]}-"                       \
        f"{date_choice.strftime('%Y%m%d')}{ini_date_choice}"               \
        f"/{var_fold_name[var_choice]}" \
        f"/{model_name[model_choice]}"   \
        f"_{var_name[var_choice]}"                        \
        f"_{date_choice.strftime('%Y%m%d')}{ini_date_choice}_{fig_name_aux1}.png"
    try:
        st.image(open(fig1, "rb").read(), format="png")  #, width=fig_width)
    except FileNotFoundError:
        st.error("ENSEMBLE: IMAGENS INDISPONÍVEIS")

elif model_choice == "PROBABILIDADE DE PRECIPITAÇÃO":

    fig1mm = f"{figs_dir}/probs-" \
        f"{date_choice.strftime('%Y%m%d')}{ini_date_choice}" \
        f"/{var_fold_name[var_choice]}" \
        f"/probs01mm" \
        f"_{var_name[var_choice]}" \
        f"_{date_choice.strftime('%Y%m%d')}{ini_date_choice}_{fig_name_aux1}.png"
    try:
        st.image(open(fig1mm, 'rb').read(), format='png')
    except FileNotFoundError:
        st.error("IMAGENS INDISPONÍVEIS")

    fig5mm = f"{figs_dir}/probs-" \
        f"{date_choice.strftime('%Y%m%d')}{ini_date_choice}" \
        f"/{var_fold_name[var_choice]}" \
        f"/probs05mm" \
        f"_{var_name[var_choice]}" \
        f"_{date_choice.strftime('%Y%m%d')}{ini_date_choice}_{fig_name_aux1}.png"
    try:
        st.image(open(fig5mm, 'rb').read(), format='png')
    except FileNotFoundError:
        st.error("IMAGENS INDISPONÍVEIS")

    fig10mm = f"{figs_dir}/probs-" \
        f"{date_choice.strftime('%Y%m%d')}{ini_date_choice}" \
        f"/{var_fold_name[var_choice]}" \
        f"/probs10mm" \
        f"_{var_name[var_choice]}" \
        f"_{date_choice.strftime('%Y%m%d')}{ini_date_choice}_{fig_name_aux1}.png"
    try:
        st.image(open(fig10mm, 'rb').read(), format='png')
    except FileNotFoundError:
        st.error("IMAGENS INDISPONÍVEIS")

    fig20mm = f"{figs_dir}/probs-" \
        f"{date_choice.strftime('%Y%m%d')}{ini_date_choice}" \
        f"/{var_fold_name[var_choice]}" \
        f"/probs20mm" \
        f"_{var_name[var_choice]}" \
        f"_{date_choice.strftime('%Y%m%d')}{ini_date_choice}_{fig_name_aux1}.png"
    try:
        st.image(open(fig20mm, 'rb').read(), format='png')
    except FileNotFoundError:
        st.error("IMAGENS INDISPONÍVEIS")

elif model_choice == 'GFS 25KM':

    date_choice_int = int(date_choice.strftime('%Y%m%d'))

    # ANOMALY MERGE

    fig1 = fig_name(figs_dir, date_choice, regions[region_choice],
                    ini_date_choice, "anom_merge", 0, 6)

    fig2 = fig_name(figs_dir, date_choice, regions[region_choice],
                    ini_date_choice, "anom_merge", 7, 13)

    fig3 = fig_name(figs_dir ,date_choice, regions[region_choice],
                    ini_date_choice, "anom_merge", 0, 14)

    try:
        st.image([open(f'{fig1}', 'rb').read(),
                  open(f'{fig2}', 'rb').read(),
                  open(f'{fig3}', 'rb').read()],
                  format='png')
    except FileNotFoundError:
        st.error("IMAGENS INDISPONÍVEIS")

    # ANOMALY CMAP
    fig4 = fig_name(figs_dir, date_choice, regions[region_choice],
                    ini_date_choice, "anom_cmap", 0, 6)

    fig5 = fig_name(figs_dir, date_choice, regions[region_choice],
                    ini_date_choice, "anom_cmap", 7, 13)

    fig6 = fig_name(figs_dir, date_choice, regions[region_choice],
                    ini_date_choice, "anom_cmap", 0, 14)

    try:
        st.image([open(f'{fig4}', 'rb').read(),
                  open(f'{fig5}', 'rb').read(),
                  open(f'{fig6}', 'rb').read()],
                  format='png')
    except FileNotFoundError:
        st.error("IMAGENS INDISPONÍVEIS")

    # ACCUMULATED
    fig7 = fig_name(figs_dir, date_choice, regions[region_choice],
                    ini_date_choice, "accum", 0, 6)

    fig8 = fig_name(figs_dir, date_choice, regions[region_choice],
                    ini_date_choice, "accum", 7, 13)

    fig9 = fig_name(figs_dir, date_choice, regions[region_choice],
                    ini_date_choice, "accum", 0, 14)

    try:
        st.image([open(f'{fig7}', 'rb').read(),
                  open(f'{fig8}', 'rb').read(),
                  open(f'{fig9}', 'rb').read()],
                  format='png')
    except FileNotFoundError:
        st.error("IMAGENS INDISPONÍVEIS")

elif model_choice == "PREVISÃO DE DOL":

    date_choice_str = str(date_choice.strftime('%Y%m%d'))
    year = date_choice_str[0:4]
    month = date_choice_str[4:6]
    day = date_choice_str[6:8]
    date_choice_int = f'{year}{int(month):02d}{int(day):02d}'
    outdir = f'{figs_dir}/dol-{date_choice_int}'

    fig1 = f"{outdir}/v850_omega850_early_{date_choice_int}.png"

    fig2 = f"{outdir}/v850_pcp_early_{date_choice_int}.png"

    fig3 = f"{outdir}/v850_pw_early_{date_choice_int}.png"

    fig4 = f"{outdir}/v850_rh_early_{date_choice_int}.png"

    fig5 = f"{outdir}/v850_sh_early_{date_choice_int}.png"

    try:
        st.image([open(f'{fig1}', 'rb').read(),
                  open(f'{fig2}', 'rb').read(),
                  open(f'{fig3}', 'rb').read()],
                  format='png')

    except FileNotFoundError:
        st.error("IMAGENS INDISPONÍVEIS")

    # try:
    #     st.image([open(f'{fig4}', 'rb').read(),
    #               open(f'{fig5}', 'rb').read()],
    #               format='png')

    # except FileNotFoundError:
    #     st.error("IMAGENS INDISPONÍVEIS")

else:

    fig1 = f"{figs_dir}/{model_name[model_choice]}-"                       \
        f"{date_choice.strftime('%Y%m%d')}{ini_date_choice}"               \
        f"/{model_version_name[model_choice]}/{var_fold_name[var_choice]}" \
        f"/{model_name[model_choice]}{model_version_name[model_choice]}"   \
        f"_gfs_kain-fritsch_{var_name[var_choice]}"                        \
        f"_{date_choice.strftime('%Y%m%d')}{ini_date_choice}_{fig_name_aux1}.png"
    try:
        st.image(open(fig1, "rb").read(), format="png")  #, width=fig_width)
    except FileNotFoundError:
        st.error("KAIN-FRITSH: IMAGENS INDISPONÍVEIS")

    fig2 = f"{figs_dir}/{model_name[model_choice]}-"                       \
        f"{date_choice.strftime('%Y%m%d')}{ini_date_choice}"               \
        f"/{model_version_name[model_choice]}/{var_fold_name[var_choice]}" \
        f"/{model_name[model_choice]}{model_version_name[model_choice]}"   \
        f"_gfs_betts-miller-janjic_{var_name[var_choice]}"                 \
        f"_{date_choice.strftime('%Y%m%d')}{ini_date_choice}_{fig_name_aux1}.png"
    try:
        st.image(open(fig2, "rb").read(), format="png")  # , width=fig_width)
    except FileNotFoundError:
        st.error("BETTS-MILLER-JANJIC: IMAGENS INDISPONÍVEIS")

    fig4 = f"{figs_dir}/{model_name[model_choice]}-"                       \
        f"{date_choice.strftime('%Y%m%d')}{ini_date_choice}"               \
        f"/{model_version_name[model_choice]}/{var_fold_name[var_choice]}" \
        f"/{model_name[model_choice]}{model_version_name[model_choice]}"   \
        f"_gfs_grell-freitas_{var_name[var_choice]}"                       \
        f"_{date_choice.strftime('%Y%m%d')}{ini_date_choice}_{fig_name_aux1}.png"
    try:
        st.image(open(fig4, "rb").read(), format="png")  # , width=fig_width)
    except FileNotFoundError:
        st.error("GRELL-FREITAS: IMAGENS INDISPONÍVEIS")

