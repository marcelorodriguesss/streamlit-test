import os
import argparse
from datetime import timedelta, datetime

from read_data import ReadData
from plotmaps import PlotMaps
import confs

class PlotVars:

    def precip(self, data, lat, lon, **kwargs):

        name_param = confs.cu_param(cu_phy)

        model_version = confs.model_version(model)

        clevs = (0., 1., 2., 3., 4., 5., 10., 15., 20.,
                25., 30., 40., 50., 60., 70., 80.)

        cmap = ('#FFFFFF', '#EDEDED', '#E1E1E1', '#4681C9', '#6A55FF',
                '#3650CB', '#1A758A', '#029A4B', '#42B338', '#83CD25',
                '#C3E711', '#FFF700', '#FF9F00', '#FF4600', '#ED0000')

        cmap_over = '#990000'

        print('\n => Precip: Acumulado diario 7h a 7h')

        diff_time, inc, maxtime = 7, 24, data.values.shape[0]

        # horários sinóticos

        print(f' => Total passos de tempo: {maxtime}')

        sinop = list(range(31, maxtime, inc))

        for s in sinop:

            try:
                curr_data = data.isel(time=s) - data.isel(time=diff_time)
                print(f"    {str(data.coords['time'][s].values)[:19]} menos " \
                    f"{str(data.coords['time'][diff_time].values)[:19]}")
            except:
                break  # sai do loop

            valid_date = (datetime.strptime(ini_date, '%d/%m/%Y %H:%M')
                        + timedelta(hours=s))

            weekday = confs.weekday(valid_date.weekday())

            fig_title = f"FUNCEME/WRF{model_version}/GFS/{name_param}\n" \
                        f"INICIALIZADA EM: {ini_date}\n" \
                        f"VÁLIDA PARA: {valid_date.strftime('%d/%m/%Y %H:%M')} ({weekday}) (GMT-3)"

            ini_date_dt = datetime.strptime(ini_date, '%d/%m/%Y %H:%M')

            if not os.path.exists(f'{args.outdir}/{model_version.lower()}/precip'):
                os.makedirs(f'{args.outdir}/{model_version.lower()}/precip')

            fig_name = f"{args.outdir}/{model_version.lower()}/precip" \
                    f"/wrf{model_version.lower()}_gfs_{name_param.lower()}" \
                    f"_preciptotal_diaria_{ini_date_dt.strftime('%Y%m%d%H')}_{int(s) - 7}h.png"

            fig_botton = 'PRECIPITAÇÃO ACUMULADA NAS ÚLTIMAS 24H'

            grid_limits = confs.grid_limits()

            kwargs = {'clevs': clevs, 'cmap': cmap, 'cmap_over': cmap_over,
                    'fig_title': fig_title, 'fig_name': fig_name,
                    'fig_botton': fig_botton, 'units': units,
                    'barinf': 'max', 'grid_limits': grid_limits}

            plotmaps.plotmaps(curr_data, data.coords['lat'],
                            data.coords['lon'], **kwargs)

            diff_time += inc
