import xarray as xr
from pandas import date_range, to_datetime

class ReadData:

    def read_data(self, var, dap_paths):

        with xr.open_dataset(dap_paths[0], decode_times=False) as dset:

            # remove as primeiras 3 horas para ficar gmt-3
            self.data_0 = dset[var][3:]
            self.title_0  = dset.attrs['TITLE']
            self.cu_phy_0 = dset.attrs['CU_PHYSICS']

            # vars comuns para todos os dados
            self.ntimes = self.data_0.shape[0]
            self.lats   = self.data_0.coords['XLAT'][0, :, 0].values
            self.lons   = self.data_0.coords['XLONG'][0, 0, :].values
            self.units  = self.data_0.attrs['units']

            # retorna string da data de inicialiação do modelo
            # no formato DD/MM/YYYY HH:MM
            self.ini_date = to_datetime(dset.attrs['START_DATE'][:10]) \
                .strftime('%d/%m/%Y %H:%M')

            # redefinir eixo do tempo
            self.time_axis = date_range(self.ini_date[:10],
                                        periods=self.ntimes,
                                        freq='H')

            # cria um novo DataArray com as dims corretas
            # e eixo do tempo correto
            self.data_0 = xr.DataArray(self.data_0.values,
                                    dims=['time', 'lat', 'lon'],
                                    coords=[self.time_axis,
                                            self.lats,
                                            self.lons])

        with xr.open_dataset(dap_paths[1], decode_times=False) as dset:

            # remove as primeiras 3 horas para ficar gmt-3
            self.data_1 = dset[var][3:]
            self.title_1  = dset.attrs['TITLE']
            self.cu_phy_1 = dset.attrs['CU_PHYSICS']

            self.data_1 = xr.DataArray(self.data_1.values,
                                       dims=['time', 'lat', 'lon'],
                                       coords=[self.time_axis,
                                               self.lats,
                                               self.lons])

        with xr.open_dataset(dap_paths[2], decode_times=False) as dset:

            # remove as primeiras 3 horas para ficar gmt-3
            self.data_2 = dset[var][3:]
            self.title_2  = dset.attrs['TITLE']
            self.cu_phy_2 = dset.attrs['CU_PHYSICS']

            self.data_2 = xr.DataArray(self.data_2.values,
                                       dims=['time', 'lat', 'lon'],
                                       coords=[self.time_axis,
                                               self.lats,
                                               self.lons])

        with xr.open_dataset(dap_paths[3], decode_times=False) as dset:

            # remove as primeiras 3 horas para ficar gmt-3
            self.data_3 = dset[var][3:]
            self.title_3  = dset.attrs['TITLE']
            self.cu_phy_3 = dset.attrs['CU_PHYSICS']

            self.data_3 = xr.DataArray(self.data_3.values,
                                       dims=['time', 'lat', 'lon'],
                                       coords=[self.time_axis,
                                               self.lats,
                                               self.lons])

        return {'model_0': [self.data_0, self.title_0, self.cu_phy_0],
                'model_1': [self.data_1, self.title_1, self.cu_phy_1],
                'model_2': [self.data_2, self.title_2, self.cu_phy_2],
                'model_3': [self.data_3, self.title_3, self.cu_phy_3]}
